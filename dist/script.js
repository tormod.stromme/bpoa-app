$('.ui.search')
  .search({
    type          : 'category',
    minCharacters : 3,
    debug: true,
    verbose: true,
    apiSettings   : {
      onResponse: function(doajResponse) {
        var
          response = {
            results : {}
          }
        ;
        // translate GitHub API response to work with search
        $.each(doajResponse.results, function(index, item) {
          var
            publisher   = item.bibjson.country || 'Unknown',
            maxResults = 20
          ;
          if(index >= maxResults) {
            return false;
          }
          // create new language category
          if(response.results[publisher] === undefined) {
            response.results[publisher] = {
              name    : publisher,
              results : []
            };
          }
          if(item.bibjson.apc !== undefined) {
            var price    = item.bibjson.apc.average_price,
                currency = item.bibjson.apc.currency
              ;
          }
          else{
            var price    = 'Ukjent',
                currency = 'APC'
              ;
          }
        
          // add result to category
          response.results[publisher].results.push({
            title       : item.bibjson.title,
            issn        : item.bibjson.identifier[0].id,
            description : item.bibjson.publisher + '<br><i>' + item.bibjson.keywords + '</i>',
            publisher   : item.bibjson.publisher,
            keywords    : item.bibjson.keywords,
            price       : price + ' ' + currency,
            title_url   : item.bibjson.link[0].url,
            country     : item.bibjson.country,
            oa_status   : 0
          });
        });
        return response;
      },
      url: 'https://doaj.org/api/v1/search/journals/{query}'
    },
    onResponse : function() {

    },
    onSelect: function(result, response) {
        var skjemaker = 'https://skjemaker.app.uib.no/view.php?id=2392527',
            country   = result.country.toLowerCase(),
            html      = ''
        ;

        html += '<div class="ui basic segment">';

        if(result.oa_status == 0) {
          html += ''
            + '<button class="ui green huge button">Open Acess</button>' 
          ;
        }
        else {
          html += ''
            + '<button class="ui red huge button">Hybrid Acess</button>' 
          ;
        }

        /*if(result.price == 'Ukjent APC') {
          html += ''
            + '<button class="ui green basic huge button">Ukjent APC</button>'
          ;
        }
        else {
          html += ''
            + '<button class="ui green basic huge button">100% av ' + result.price + ' dekkes</button>'
          ;
        }*/

        html += ''
          + '<h1 class="ui header"><span class="flag-icon flag-icon-' + country + '"></span></i> <a target="_blank" href="' + result.title_url + '">' + result.title + ' <i class="external icon"></i></a><div class="sub header">' + result.publisher + '</div></h1>'
          + '<p>' + result.issn + '</p>'
        ;

        if(result.keywords !== undefined) {
          html += ''
            + '<div class="journal-keywords">'
          ;
        }
        $.each(result.keywords, function(index, keyword) {
          html += ''
            + '<a class="ui tag label">' + keyword + '</a>'
          ;
        });
        if(result.keywords !== undefined) {
          html += ''
            + '</div>'
          ;
        }

        html += ''
          + '<div class="apc">100% av den estimerte kostnaden på ' + result.price + ' dekkes <i class="asterisk red icon"></i></div>'
          + '<div class="apc caveat"><i class="asterisk red icon"></i> du må selv sjekke og registrere den summen du har fått oppgitt fra forlaget</div>'
        ;

/*        html += ''
          + '<i class="chevron down huge icon"></i>'
          + '<div class="ui horizontal divider">Sjekk DBH nivå på tidsskriftet</div>'  
          + '<form class="ui equal width form">'
          +      '<div class="field">'
          +        '<div class="ui checkbox">'
          +        '<a class="ui red basic fluid button" target="_blank" href="https://dbh.nsd.uib.no/publiseringskanaler/KanalTreffliste.action?enkeltSok=' + result.title + '&__checkbox_bibsys=true&sok;.avansert=false&treffliste;.tidsskriftTreffside=1&treffliste;.forlagTreffside=1&treffliste;.vis=true">' + 'Sjekk hvilket nivå ' + result.title + ' har i DBH  <i class="external icon"></i></a>'
          +        '</div>'
          +      '</div>'
          +      '<div class="inline required field">'
          +        '<div class="ui selection dropdown">'
          +        '<input type="hidden" name="dbh_level">'
          +        '<i class="dropdown icon"></i>'
          +        '<div class="default text">Sett DBH nivå</div>'
          +        '<div class="menu">'
          +          '<div class="item" data-value="1">1</div>'
          +          '<div class="item" data-value="2">2</div>'
          +        '</div>'
          +      '</div>'
          +    '</div>'
          +    '<div class="required huge field">'
          +      '<div class="ui huge checkbox">'
          +        '<input type="checkbox" name="DBH_statement">'
          +        '<label>Jeg bekrefter at jeg har sjekket <a href="http://nsd.no">DBH</a>?</label>'
          +      '</div>'
          +    '</div>'
          +    '<div class="ui horizontal divider">Gå til neste steg</div>'  
        ;*/

        html += ''
          +    '<a class="ui primary huge submit button" href="' + skjemaker 
          +      '&element_80=1' // ARTIKKEL
          +      '&element_6=' + result.title // JOURNAL
          +      '&element_18=' + result.publisher // UTGIVER
          +      '&element_22=' + result.title_url // UTGIVER/JOURNAL URL
          +      '">Søk publiseringsstøtte til open access artikkel</a>'
        ;

        html += '</div>';

        $('.journal-choice').html(html);
        return 'hide results';

      }
  })
;


$('.ui.dropdown')
  .dropdown()
;

$('.ui.form')
  .form({
    fields: {
      gender   : 'minLength[1]',
      DBH_statement    : 'checked'
    }
  })
;